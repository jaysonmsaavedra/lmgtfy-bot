const Discord = require('discord.js')
const client = new Discord.Client()
const lmgtfyLink = 'http://lmgtfy.com/?q='
require('dotenv').config()

client.on('ready', () => {
    console.log('LMGTFY Bot ready')
})

client.on('message', message => {
    if(message.content.startsWith('!lmgtfy')) {
        if(message.content.length == 7) {
            // return lmgtfy link of message before command
            message.channel.fetchMessages({limit: 2})
            .then(messages => {
                message.channel.send(`${lmgtfyLink}${messages.last().content.split(' ').join('+')}`)
            })
            .catch(err => console.log(err))
        } else {
            // return lmgtfy link of the content of the message issued with the command
            message.channel.send(`${lmgtfyLink}${message.content.slice(8).split(' ').join('+')}`)
        }
    }
})

client.on('messageReactionAdd', messageReaction => {
    console.log(messageReaction.emoji.name)
    if(messageReaction.emoji.name === 'lmgtfy') {
        messageReaction.message.channel.send(`${lmgtfyLink}${messageReaction.message.content.split(' ').join('+')}`)
    }
})

client.login(process.env.TOKEN)